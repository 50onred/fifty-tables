from setuptools import find_packages, setup

setup(
    name='Fifty-Tables',
    version='1.9.3',
    url='https://bitbucket.org/50onred/fifty-tables/overview',
    author='Steve Dorazio',
    author_email='sdorazio@50onred.com',
    description='Generic Tables Framework',
    long_description=__doc__,
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'flask',
        'Fifty-Flask',
        'SQLAlchemy',
        'wtforms',
        'six',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
