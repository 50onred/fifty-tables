from flask import url_for
from six import iteritems

from ..compat import base_string_type as basestring

def url_for_row(row, url=None, endpoint=None, url_params=None, default_url_params=None):
    if not url and not endpoint:
        return '#'
    if isinstance(url_params, list):
        url_param_vals = {v: row.get(v) for v in url_params}
    elif isinstance(url_params, dict):
        url_param_vals = {k: row.get(v) for k, v in iteritems(url_params)}
    elif isinstance(url_params, basestring):
        url_param_vals = {url_params: row.get(url_params)}
    else:
        url_param_vals = {}

    if isinstance(default_url_params, dict):
        for k, v in iteritems(default_url_params):
            url_param_vals.setdefault(k, v)
    elif isinstance(default_url_params, list):
        for v in default_url_params:
            url_param_vals.setdefault(v, v)
    elif default_url_params is not None:
        url_param_vals.setdefault(default_url_params, default_url_params)

    if endpoint:
        return url_for(endpoint, **url_param_vals)
    else:
        return url.format(**url_param_vals)


def format_optional_decimal(value, int_format='{:,}', decimal_format='{:,.2f}'):
    if value is None:
        return value
    elif isinstance(value, basestring):
        return value
    return int_format.format(int(value)) if value % 1 == 0 else decimal_format.format(value)


def format_currency_optional_decimal(value, int_format='${:,}', decimal_format='${:,.2f}'):
    if value is None:
        return value
    elif isinstance(value, basestring):
        return value
    elif value < 0:
        return format_currency_optional_decimal((value * -1), int_format='-' + int_format, decimal_format='-' + decimal_format)
    return int_format.format(int(value)) if value % 1 == 0 else decimal_format.format(value)
