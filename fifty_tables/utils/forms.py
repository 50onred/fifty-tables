from six import iteritems
from wtforms.validators import Length, NumberRange, Email, EqualTo, InputRequired, Required, DataRequired, Optional, Regexp, URL, AnyOf
import re


def add_parsley_field_attributes(field, attributes=None, default_trigger=u'focusout'):
    """
    Adds the data attributes for parsley based on the field's valdators.

    NOTE: Existing values in {attributes} take higher priority.

    :param field:           the form field to use
    :param attributes:      the attribute dictionary to populate
    :param default_trigger: the default trigger to use
    :return: the populated attributes
    """
    attributes = attributes or {}
    if attributes.get('data-no-parsley') == 'true':
        # Disable parsley validation
        return attributes

    parsley_attributes = get_parsley_attributes(field, default_trigger)
    for attribute, value in iteritems(parsley_attributes):
        # Update the attributes that do not already exist
        attributes.setdefault(u'data-'+attribute, value)
    return attributes


def get_parsley_attributes(field, default_trigger=u'focusout'):
    """ Gets the parsley data attributes for the given field """
    parsley_attributes = {}
    if field.validators:
        for validator in field.validators:
            if isinstance(validator, InputRequired) or isinstance(validator, DataRequired) or isinstance(validator, Required):
                parsley_attributes[u'parsley-required'] = u'true'
                _message_parsley_attributes(parsley_attributes, u'parsley-required', validator)
            elif isinstance(validator, Email):
                parsley_attributes[u'parsley-type'] = u'email'
                _message_parsley_attributes(parsley_attributes, u'parsley-type', validator)
            elif isinstance(validator, EqualTo):
                parsley_attributes[u'parsley-equalto'] = u'input[name=\'' + validator.fieldname + '\']'
                _message_parsley_attributes(parsley_attributes, u'parsley-equalto', validator)
            elif isinstance(validator, Length):
                if validator.min != -1:
                    parsley_attributes[u'parsley-minlength'] = str(validator.min)
                    _message_parsley_attributes(parsley_attributes, u'parsley-minlength', validator)
                if validator.max != -1:
                    parsley_attributes[u'parsley-maxlength'] = str(validator.max)
                    _message_parsley_attributes(parsley_attributes, u'parsley-maxlength', validator)
            elif isinstance(validator, NumberRange):
                parsley_attributes[u'parsley-type'] = u'number'
                _message_parsley_attributes(parsley_attributes, u'parsley-type', validator)
                # NOTE - Parsley Range validator is written by idiots. Logic of (!min||!max) throws Error with 0 min value.
                if validator.min is not None:
                    parsley_attributes[u'parsley-min'] = str(validator.min)
                    _message_parsley_attributes(parsley_attributes, u'parsley-min', validator)
                elif validator.max is not None:
                    parsley_attributes[u'parsley-max'] = str(validator.max)
                    _message_parsley_attributes(parsley_attributes, u'parsley-max', validator)
            elif isinstance(validator, URL):
                parsley_attributes[u'parsley-type'] = u'url'
                _message_parsley_attributes(parsley_attributes, u'parsley-type', validator)
            elif isinstance(validator, AnyOf):
                _anyof_parsley_attributes(parsley_attributes, validator)
            elif isinstance(validator, Regexp):
                _regexp_parsley_attributes(parsley_attributes, validator)
            elif hasattr(validator, '_parsley_validator_name'):
                # Allow validator to specify the parsley validator to use
                value = validator._parsley_validator_value if hasattr(validator, '_parsley_validator_value') else 'true'
                parsley_attributes[validator._parsley_validator_name] = value
                _message_parsley_attributes(parsley_attributes, validator._parsley_validator_name, validator)

            # Attach the default trigger to the attribute
            if default_trigger and not u'data_trigger' in parsley_attributes:
                parsley_attributes[u'parsley-trigger'] = default_trigger
    return parsley_attributes


def _regexp_parsley_attributes(parsley_attributes, validator):
    # Apparently, this is the best way to check for RegexObject Type
    # It's needed because WTForms allows compiled regexps to be passed to the validator
    RegexObject = type(re.compile(''))
    if isinstance(validator.regex, RegexObject):
        regex_string = validator.regex.pattern
    else:
        regex_string = validator.regex
    parsley_attributes[u'parsley-regexp'] = regex_string
    _message_parsley_attributes(parsley_attributes, u'parsley-regexp', validator)


def _string_seq_delimiter(parsley_attributes, validator):
    # We normally use a comma as the delimiter - looks clean and it's parsley's default.
    # If the strings for which we check contain a comma, we cannot use it as a delimiter.
    default_delimiter = u','
    fallback_delimiter = u';;;'
    delimiter = default_delimiter
    for value in validator.values:
        if value.find(',') != -1:
            delimiter = fallback_delimiter
            break
    if delimiter != default_delimiter:
        parsley_attributes[u'parsley-inlist-delimiter'] = delimiter
    return delimiter


def _anyof_parsley_attributes(parsley_attributes, validator):
    delimiter = _string_seq_delimiter(parsley_attributes, validator)
    parsley_attributes[u'parsley-inlist'] = delimiter.join(validator.values)


def _trigger_parsley_attributes(parsley_attributes, trigger=u'change'):
    parsley_attributes[u'parsley-trigger'] = trigger


def _message_parsley_attributes(parsley_attributes, constraint, validator):
    if hasattr(validator, 'message') and validator.message:
        parsley_attributes[u'{}-message'.format(constraint)] = validator.message
