from datetime import date
from decimal import Decimal

from six import iteritems
from sqlalchemy import asc, desc
from sqlalchemy.orm import RelationshipProperty
import re
import warnings


def request_to_dict(values):
    result = {}
    for k, v in iteritems(values):
        result[k] = v
    return result


def result_to_list(rows, keep_fields=[]):
    """ ResultProxy to json-serializable array helper """
    return [row for row in result_to_dict_gen(rows, keep_fields)]


def result_to_dict_gen(rows, keep_fields=[]):
    for row in rows:
        mutated = {}
        for key, item in iteritems(dict(row)):
            if len(keep_fields) > 0 and key not in keep_fields:
                continue
            if isinstance(item, Decimal):
                item = float(item)
            if isinstance(item, date):
                item = '%s' % item
            mutated[key] = item
        yield mutated


def is_valid_column_name(col):
    """ Returns false if col contains non alpha numeric characters, underscores """
    return not re.search('[^a-zA-Z0-9_]', col)


def get_page_perpage(params, default_page=1, default_perpage=10):
    page = abs(int(params.get('page', default_page))) if params.get('page', default_page) else None
    perpage = abs(int(params.get('perpage', default_perpage))) if params.get('perpage', default_perpage) else None
    return page, perpage


##############################
# SQLAlchemy query functions #
##############################


def get_fifty_table_result_from_query(query, params={}, default_page=1, default_perpage=10,
                                      default_sort=None, default_sort_direction='asc',
                                      sort_classes=[], sort_attrs={}, row_generator=None, keep_fields=[],
                                      footer_rows=[], subtotal_row=None, totals_row=None, grand_totals_row=None):
    """ Gets a paginated FiftyTableResult object for a SQLAlchemy query  """
    from ..result import FiftyTableResult
    warnings.warn('Method to be removed. Use view mixin instead.', DeprecationWarning, stacklevel=2)
    sorted_query = get_sorted_query(query, params, default_sort=default_sort, default_sort_direction=default_sort_direction,
                                    sort_classes=sort_classes, sort_attrs=sort_attrs)

    paginated_query, page, perpage = get_paginated_query(sorted_query, params,
                                                         default_page=default_page,
                                                         default_perpage=default_perpage)

    # Get all of the items from the query
    items = paginated_query.all()

    # Generate the dictionaries of all the objects
    row_generator = row_generator or sqlalchemy_obj_generator
    rows = [d for d in row_generator(items, keep_fields=keep_fields)]

    # Get the total number of items matching the query
    if page == 1 and len(rows) < perpage:
        total_rows = len(rows)
    else:
        total_rows = query.count()

    # Add in the footer rows (if present)
    table_footer_rows = footer_rows or []
    if subtotal_row:
        table_footer_rows.append(subtotal_row)
    if totals_row:
        table_footer_rows.append(totals_row)
    if grand_totals_row:
        table_footer_rows.append(grand_totals_row)

    # Get the sort property/direction from the parameters
    if params.get('order'):
        order = params.get('order').split('__')
        sort_property = order[0]
        sort_direction = order[1]
    else:
        sort_property = default_sort
        sort_direction = default_sort_direction

    return FiftyTableResult(rows=rows, footer_rows=table_footer_rows, page=page, perpage=perpage,
                            total_rows=total_rows, sort_property=sort_property,
                            sort_direction=sort_direction)


def get_sorted_query(query, params, default_sort=None, default_sort_direction='asc', sort_classes=[], sort_attrs={}):
    """
    Applies the order clause to the given query.

    :param query: The SQLALchemy query object
    :param params: The request parameters as a dictionary
    :param default_sort: The default sort property.
    :param default_sort_direction: The default sort direction ('asc' or 'desc')
    :param sort_classes: The classes to obtain the the sort column from. Example: 'last_name' with [User] will use User.last_name
    :param sort_attrs:  A static mapping of attributes to apply to. Example: {'last_name': User.last_name}.
    :return: The query with the sort parameters applied.
    """
    warnings.warn('Method to be removed. Use view mixin instead.', DeprecationWarning, stacklevel=2)
    order = params.get('order') or default_sort
    if order and order.startswith('undefined'):
        order = default_sort

    if not order:
        return query

    sort_criterion = []
    for order_property in order.split(','):
        sort_property = order_property
        sort_func = desc if default_sort_direction == 'desc' else asc
        parts = order.split('__')
        if len(parts) == 2 and parts[1] in ('asc', 'desc') and is_valid_column_name(parts[0]):
            sort_property = parts[0]
            sort_func = desc if parts[1] == 'desc' else asc

        # Check to see if we have a static mapping (ex. 'advertiser': Advertiser.last_name)
        if sort_attrs and sort_property in sort_attrs and sort_attrs[sort_property] is not None:
            sort_criterion.append(sort_func(sort_attrs[sort_property]))
            continue

        # Check to see if we need the class to go with it (ex. 2 columns named 'id' in the same query)
        if sort_classes:
            for cls in sort_classes:
                if hasattr(cls, sort_property):
                    sort_criterion.append(sort_func(getattr(cls, sort_property)))
                    break
        else:
            sort_criterion.append(sort_func(sort_property))
    if sort_criterion:
        return query.order_by(*sort_criterion)
    return query


def get_paginated_query(query, params, default_page=1, default_perpage=10):
    """ Applies pagination to the query """
    warnings.warn('Method to be removed. Use view mixin instead', DeprecationWarning, stacklevel=2)
    page, perpage = get_page_perpage(params, default_page, default_perpage)
    if page is not None:
        return query.limit(perpage).offset((page - 1) * perpage), page, perpage
    return query, page, perpage


def sqlalchemy_obj_generator(items,  keep_fields=[], exclude_fields=[]):
    """ Generator for sqlalchemy_obj_to_dict """
    warnings.warn('Method to be removed. Use view mixin instead.', DeprecationWarning, stacklevel=2)
    for item in items:
        yield sqlalchemy_obj_to_dict(item, keep_fields=keep_fields, exclude_fields=exclude_fields)


def sqlalchemy_obj_to_dict(obj, keep_fields=[], exclude_fields=[]):
    """
    Converts a SQLAlchemy object to a flat dictionary.

    :param obj: The SQLAlchemy model or result object
    :param keep_fields: The fields to keep. By default, all public fields are kept except relationship properties.
    :param exclude_fields: The fields to exclude.
    :return: A dictionary object with the form { 'property or label': value }
    """
    warnings.warn('Method to be removed. Use view mixin instead.', DeprecationWarning, stacklevel=2)
    mutated = {}
    for key, item in iteritems(obj.__dict__):
        if len(keep_fields) > 0 and key not in keep_fields:
            continue
        elif key.startswith('_'):
            continue
        elif exclude_fields and key in exclude_fields:
            continue
        elif hasattr(obj.__class__, key) and hasattr(getattr(obj.__class__, key), 'property') \
                and isinstance(getattr(obj.__class__, key).property, RelationshipProperty):
            # Ignore relationship properties automatically
            continue
        elif isinstance(item, Decimal):
            item = float(item)
        elif isinstance(item, date):
            item = '%s' % item
        mutated[key] = item
    return mutated
