
class ImageMixin(object):

    def __init__(self, name, img_width=None, img_height=None, cell_template='_tables/cells/image_cell.html', sortable=False, **kwargs):
        super(ImageMixin, self).__init__(name=name, img_width=img_width, img_height=img_height,
                                         cell_template=cell_template, sortable=sortable, **kwargs)
        self.img_width = img_width
        self.img_height = img_height
        self.column_classes.append('cell-image')

    def get_image_url(self, row, **kwargs):
        return self.get_value(row, **kwargs)

    def get_image_style(self, row, **kwargs):
        style_attrs = []
        if self.img_width is not None:
            style_attrs.append('width: {}'.format(self.img_height))
        if self.img_height is not None:
            style_attrs.append('height: {}'.format(self.img_height))
        return '; '.join(style_attrs)



class ClickableImageMixin(ImageMixin):

    def __init__(self, name, cell_template='_tables/cells/clickable_image_cell.html', **kwargs):
        super(ClickableImageMixin, self).__init__(name=name, cell_template=cell_template, **kwargs)
        self.column_classes.append('cell-clickableimage')

    def get_clickable_image_url(self, row, **kwargs):
        return self.get_image_url(row, **kwargs)

