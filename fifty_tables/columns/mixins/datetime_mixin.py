from datetime import datetime, date
from fifty_tables.compat import base_string_type as basestring


class DateMixin(object):

    def __init__(self, name, date_format='%m/%d/%Y', date_data_format='%Y-%m-%d',
                 default_sort_direction='desc', **kwargs):
        super(DateMixin, self).__init__(name=name, date_format=date_format, date_data_format=date_data_format,
                                        default_sort_direction=default_sort_direction, **kwargs)
        self.date_format = date_format
        self.date_data_format = date_data_format
        self.column_classes.append('cell-date')

    def get_value(self, row, **kwargs):
        value = self.get_raw_value(row, **kwargs)
        if value is None:
            return self.null_value
        elif isinstance(value, basestring) and len(value) > 3:
            return self.format_date(self.parse_date_str(value))
        elif isinstance(value, date):
            return self.format_date(value)
        return value

    def get_cell_attributes(self, row, **kwargs):
        _attrs = super(DateMixin, self).get_cell_attributes(row, **kwargs)
        _date = self.get_raw_value(row, **kwargs)
        if isinstance(_date, basestring):
            _attrs['data-value'] = _date
        elif isinstance(_date, date):
            _attrs['data-value'] = self.format_date(_date)
        return _attrs

    def format_date(self, date):
        return date.strftime(self.date_format)

    def parse_date_str(self, value):
        return datetime.strptime(value, self.date_data_format)


class DateTimeMixin(DateMixin):

    def __init__(self, name, date_format='%m/%d/%Y at %I:%M%p', date_data_format='%Y-%m-%d %H:%M:%S', **kwargs):
        super(DateTimeMixin, self).__init__(name=name, date_format=date_format, date_data_format=date_data_format, **kwargs)
        self.column_classes.append('cell-datetime')
