class MicrosMixin(object):
    """ Column mixin to automatically convert from micros to the real value. """

    def get_raw_value(self, row, **kwargs):
        _value = super(MicrosMixin, self).get_raw_value(row, **kwargs)
        return _value if not _value else _value / 1000000.0