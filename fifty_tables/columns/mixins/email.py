class EmailMixin(object):

    def __init__(self, name='email', cell_template='_tables/cells/email_cell.html', **kwargs):
        super(EmailMixin, self).__init__(name=name, cell_template=cell_template, **kwargs)
        self.column_classes.append('cell-email')

    def get_email(self, row):
        return self.get_raw_value(row)

