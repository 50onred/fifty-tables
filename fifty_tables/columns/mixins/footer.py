class EmptyFooterMixin(object):
    def __init__(self, footer_template='_tables/footers/empty_table_footer.html', **kwargs):
        super(EmptyFooterMixin, self).__init__(footer_template=footer_template, **kwargs)
