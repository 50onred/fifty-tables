
class CheckboxMixin(object):

    def __init__(self, name, header_template='_tables/headers/checkbox_header.html',
                 cell_template='_tables/cells/checkbox_cell.html', checked=False,
                 shiftclick_enabled=True, select_all_enabled=True, sortable=False, **kwargs):
        super(CheckboxMixin, self).__init__(name=name, header_template=header_template, cell_template=cell_template,
                                            checked=checked, shiftclick_enabled=shiftclick_enabled,
                                            select_all_enabled=select_all_enabled, sortable=sortable, **kwargs)
        self.shiftclick_enabled = shiftclick_enabled
        self.select_all_enabled = select_all_enabled
        self.checked = checked
        self.column_classes.append('cell-checkbox')

    def get_header_attributes(self, **kwargs):
        attrs = super(CheckboxMixin, self).get_header_attributes(**kwargs)
        attrs['data-select-all-enabled'] = self.select_all_enabled
        return attrs

    def is_checked(self, row):
        return self.checked
