from ...utils import format_optional_decimal, format_currency_optional_decimal


class NumericMixin(object):

    def __init__(self, name, int_format='{:,}', decimal_format='{:,.2f}', sum_page_totals=True,
                 csv_null_value='', default_sort_direction='desc', **kwargs):
        super(NumericMixin, self).__init__(name=name, int_format=int_format, decimal_format=decimal_format,
                                           csv_null_value=csv_null_value, default_sort_direction=default_sort_direction,
                                           **kwargs)
        self.int_format = int_format
        self.decimal_format = decimal_format
        self.sum_page_totals = sum_page_totals
        self.csv_null_value = csv_null_value
        # DEPRECATED: `numeric` class kept for historic reasons, stop using it.
        self.column_classes.extend(('numeric', 'cell-numeric'))

    def get_formatted_number(self, row, **kwargs):
        _value = self.get_raw_value(row, **kwargs)
        if _value is None:
            _value = self.null_value
        return format_optional_decimal(_value, self.int_format, self.decimal_format)

    def get_csv_formatted_number(self, row, **kwargs):
        _value = self.get_raw_value(row, **kwargs)
        if _value is None:
            _value = self.csv_null_value
        return _value

    def get_value(self, row, **kwargs):
        return self.get_formatted_number(row, **kwargs)

    def get_csv_value(self, row, **kwargs):
        return self.get_csv_formatted_number(row, **kwargs)

    def get_footer_value(self, row, **kwargs):
        _value = self.get_raw_value(row, **kwargs)
        if _value is None:
            return ''
        return self.get_formatted_number(row, **kwargs)

    def get_cell_attributes(self, row, **kwargs):
        _attrs = super(NumericMixin, self).get_cell_attributes(row, **kwargs)
        _attrs['data-value'] = self.get_raw_value(row)
        return _attrs


class CurrencyMixin(NumericMixin):

    def __init__(self, name, int_format='${:,}', decimal_format='${:,.2f}', **kwargs):
        super(CurrencyMixin, self).__init__(name=name, int_format=int_format, decimal_format=decimal_format, **kwargs)
        self.column_classes.append('cell-currency')

    def get_formatted_number(self, row, **kwargs):
        _value = self.get_raw_value(row, **kwargs)
        if _value is None:
            _value = self.null_value
        return format_currency_optional_decimal(_value, self.int_format, self.decimal_format)


class PercentageMixin(NumericMixin):

    def __init__(self, name, int_format='{:,}%', decimal_format='{:,.3f}%', **kwargs):
        super(PercentageMixin, self).__init__(name=name, int_format=int_format, decimal_format=decimal_format, **kwargs)
        self.column_classes.append('cell-percentage')


