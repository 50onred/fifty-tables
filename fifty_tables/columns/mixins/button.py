
class ButtonMixin(object):

    def __init__(self, name, cell_template='_tables/cells/button_cell.html', button_classes=[],
                 button_text=None, sortable=False, **kwargs):
        super(ButtonMixin, self).__init__(name=name, cell_template=cell_template, button_classes=button_classes,
                                          button_text=button_text, sortable=sortable, **kwargs)
        self.button_classes = button_classes
        self.button_text = button_text
        self.column_classes.append('cell-button')

    def get_button_classes(self, row, **kwargs):
        return self.button_classes

    def get_button_attributes(self, row, **kwargs):
        return {}

    def get_button_text(self, row, **kwargs):
        return self.button_text if self.button_text is not None else self.get_value(row, **kwargs)


class LinkButtonMixin(ButtonMixin):

    def __init__(self, name='', button_classes=['btn-link'], **kwargs):
        super(LinkButtonMixin, self).__init__(name=name, button_classes=button_classes, **kwargs)

