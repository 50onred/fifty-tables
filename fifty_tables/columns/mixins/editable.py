from ...utils import url_for_row, get_parsley_attributes
import json


#
# Editable Columns - VERY UGLY
#
class EditableMixin(object):

    def __init__(self, name, cell_template='_tables/cells/editable_cell.html',
                 editable_type='text', editable_data_format='text', editable_pk_property='id',
                 editable_display_text_format=None, editable_empty_text=None,
                 editable_empty_text_format=None, editable_empty_text_property=None,
                 editable_on_success=None, editable_url=None, editable_endpoint=None,
                 editable_url_params={}, editable_default_url_params={}, editable_form_field=None,
                 footer_template='_tables/footers/empty_table_footer.html',
                 **kwargs):
        super(EditableMixin, self).__init__(name=name, cell_template=cell_template, editable_type=editable_type,
                                            editable_data_format=editable_data_format,
                                            editable_pk_property=editable_pk_property,
                                            editable_display_text_format=editable_display_text_format,
                                            editable_empty_text=editable_empty_text,
                                            editable_empty_text_property=editable_empty_text_property,
                                            editable_empty_text_format=editable_empty_text_format,
                                            editable_on_success=editable_on_success,
                                            editable_url=editable_url,
                                            editable_endpoint=editable_endpoint,
                                            editable_url_params=editable_url_params,
                                            editable_default_url_params=editable_default_url_params,
                                            editable_form_field=editable_form_field,
                                            footer_template=footer_template, **kwargs)
        self.editable_type = editable_type
        self.editable_data_format = editable_data_format
        self.editable_pk_property = editable_pk_property
        self.editable_display_text_format = editable_display_text_format
        self.editable_empty_text = editable_empty_text
        self.editable_empty_text_format = editable_empty_text_format
        self.editable_empty_text_property = editable_empty_text_property
        self.editable_on_success = editable_on_success
        self.editable_url = editable_url
        self.editable_endpoint = editable_endpoint
        self.editable_url_params = editable_url_params
        self.editable_default_url_params = editable_default_url_params
        self.editable_form_field = editable_form_field
        self.column_classes.append('cell-editable')

    def get_editable_type(self, row, **kwargs):
        return self.editable_type

    def get_editable_url(self, row, **kwargs):
        return url_for_row(row=row, url=self.editable_url, endpoint=self.get_editable_endpoint(row, **kwargs),
                           url_params=self.get_editable_url_params(row, **kwargs),
                           default_url_params=self.editable_default_url_params)

    def get_editable_endpoint(self, row, **kwargs):
        return self.editable_endpoint

    def get_editable_url_params(self, row, **kwargs):
        return self.editable_url_params

    def get_editable_pk_property(self, row, **kwargs):
        return self.editable_pk_property

    def get_editable_data_format(self, row, **kwargs):
        return self.editable_data_format

    def get_editable_container_attributes(self, row, **kwargs):
        return {
            'data-ui-editable': True,
            'data-format': self.get_editable_data_format(row, **kwargs)
        }

    def get_editable_container_classes(self, row, **kwargs):
        return ['edit-container']

    def get_editable_classes(self, row, **kwargs):
        classes = ['editable', self.property_str]
        if self.get_raw_value(row, **kwargs) is None:
            classes.append('editable-empty')
        return classes

    def get_editable_attributes(self, row, **kwargs):
        attrs = {
            'data-type': self.get_editable_type(row, **kwargs),
            'data-name': self.property_str,
            'data-url': self.get_editable_url(row, **kwargs),
            'data-value': self.get_raw_value(row)
        }

        pk_property = self.get_editable_pk_property(row, **kwargs)
        if pk_property:
            attrs['data-pk'] = row.get(pk_property)

        _empty_text = self.get_editable_empty_text(row, **kwargs)
        if _empty_text is not None:
            attrs['data-default-value'] = _empty_text

        _validators = self.get_editable_validators(row, **kwargs)
        if _validators is not None:
            attrs['data-editable-validators'] = json.dumps(_validators)

        if self.editable_on_success:
            attrs['data-on-success'] = self.editable_on_success
        return attrs

    def get_editable_empty_text(self, row):
        value = None
        if self.editable_empty_text_property:
            value = row.get(self.editable_empty_text_property, self.editable_empty_text)
        elif self.editable_empty_text:
            value = self.editable_empty_text
        if value is not None and self.editable_empty_text_format:
            value = self.editable_empty_text_format.format(value)
        return value

    def get_editable_validators(self, row, **kwargs):
        validators = {'parsley-validate': 'true'}
        if self.editable_form_field is not None:
            validators.update(get_parsley_attributes(self.editable_form_field))
        return validators

    def get_display_value(self, row, **kwargs):
        value = self.get_raw_value(row)
        if value is None:
            return self.get_editable_empty_text(row) or ''
        if self.editable_display_text_format:
            return self.editable_display_text_format.format(value)
        return value
