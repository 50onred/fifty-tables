from flask import current_app, template_rendered


class TooltipMixin(object):

    def __init__(self, name, cell_template='_tables/cells/base_table_cell.html',
                 tooltip_cell_template='_tables/cells/tooltip_cell.html',
                 tooltip_content_template=None, tooltip_trigger=None,
                 tooltip_delay=None, tooltip_icon='fa fa-info-circle', **kwargs):
        super(TooltipMixin, self).__init__(name=name, cell_template=tooltip_cell_template,
                                           tooltip_trigger=tooltip_trigger,
                                           tooltip_delay=tooltip_delay, tooltip_icon=tooltip_icon,
                                           tooltip_content_template=tooltip_content_template, **kwargs)
        self.tooltip_base_cell_template = cell_template
        self.tooltip_trigger = tooltip_trigger
        self.tooltip_delay = tooltip_delay
        self.tooltip_icon = tooltip_icon
        self.tooltip_content_template = tooltip_content_template
        self.column_classes.append('cell-tooltip')

    def get_cell_context(self, **kwargs):
        context = super(TooltipMixin, self).get_cell_context(**kwargs)
        context['tooltip_base_cell_template'] = self.tooltip_base_cell_template
        context['tooltip_icon'] = self.tooltip_icon
        return context

    def has_tooltip(self, row, **kwargs):
        return True

    def get_tooltip_attributes(self, row, **kwargs):
        attrs = {
            'data-container': 'body',
            'data-toggle': 'tooltip',
            'data-html': True,
            'data-title': self.get_tooltip_content(row, **kwargs)
        }
        if self.tooltip_trigger:
            attrs['data-trigger'] = self.tooltip_trigger
        if self.tooltip_delay:
            attrs['data-delay'] = self.tooltip_delay
        return attrs

    def get_tooltip_content(self, row, **kwargs):
        return self.render_tooltip_content(row=row, **kwargs) if self.tooltip_content_template else ''

    def get_tooltip_context(self, **kwargs):
        return self.get_base_context(**kwargs)

    def render_tooltip_content(self, row, **kwargs):
        context = self.get_tooltip_context(row=row, **kwargs)
        template = current_app.jinja_env.get_or_select_template(self.tooltip_content_template)
        rv = template.render(context)
        template_rendered.send(current_app._get_current_object(), template=template, context=context)
        return rv


class PropertyTooltipMixin(TooltipMixin):

    def __init__(self, name, tooltip_content_template='_tables/tooltips/property_tooltip.html',
                 tooltip_property_label='', tooltip_property='', **kwargs):
        super(PropertyTooltipMixin, self).__init__(name=name, tooltip_content_template=tooltip_content_template,
                                                   tooltip_property_label=tooltip_property_label,
                                                   tooltip_property=tooltip_property, **kwargs)
        self.tooltip_property_label = tooltip_property_label
        self.tooltip_property = tooltip_property

    def has_tooltip(self, row, **kwargs):
        return row.get(self.tooltip_property) is not None

    def get_tooltip_property_value(self, row, **kwargs):
        return row.get(self.tooltip_property)


class HeaderTooltipMixin(object):
    def __init__(self, name, header_template='_tables/headers/tooltip_header.html',
                 header_tooltip_content=None, header_tooltip_content_template=None,
                 header_tooltip_trigger=None, header_tooltip_delay=None,
                 header_tooltip_icon='fa fa-info-circle', **kwargs):
        super(HeaderTooltipMixin, self).__init__(name=name, header_template=header_template,
                                                 header_tooltip_content=header_tooltip_content,
                                                 header_tooltip_content_template=header_tooltip_content_template,
                                                 header_tooltip_trigger=header_tooltip_trigger,
                                                 header_tooltip_delay=header_tooltip_delay,
                                                 header_tooltip_icon=header_tooltip_icon, **kwargs)
        self.header_tooltip_content = header_tooltip_content
        self.header_tooltip_content_template = header_tooltip_content_template
        self.header_tooltip_icon = header_tooltip_icon
        self.header_tooltip_trigger = header_tooltip_trigger
        self.header_tooltip_delay = header_tooltip_delay

    def has_header_tooltip(self):
        return self.header_tooltip_content or self.header_tooltip_content_template

    def get_header_tooltip_attributes(self, **kwargs):
        attrs = {
            'data-container': 'body',
            'data-toggle': 'tooltip',
            'data-html': True,
            'data-title': self.get_header_tooltip_content(**kwargs)
        }
        if self.header_tooltip_trigger:
            attrs['data-trigger'] = self.header_tooltip_trigger
        if self.header_tooltip_delay:
            attrs['data-delay'] = self.header_tooltip_delay
        return attrs

    def get_header_context(self, **context):
        context = super(HeaderTooltipMixin, self).get_header_context(**context)
        context['tooltip_icon'] = self.get_header_tooltip_icon()
        return context

    def get_header_tooltip_icon(self, **kwargs):
        return self.header_tooltip_icon

    def get_header_tooltip_content(self, **kwargs):
        if self.header_tooltip_content is not None:
            return self.header_tooltip_content
        return self.render_header_tooltip_content()

    def get_header_tooltip_context(self, **kwargs):
        return self.get_header_context(**kwargs)

    def render_header_tooltip_content(self, row, **kwargs):
        context = self.get_header_tooltip_context(row=row, **kwargs)
        template = current_app.jinja_env.get_or_select_template(self.header_tooltip_content_template)
        rv = template.render(context)
        template_rendered.send(current_app._get_current_object(), template=template, context=context)
        return rv
