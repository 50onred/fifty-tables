from ...utils import url_for_row


class SelectMixin(object):

    def __init__(self, name, cell_template='_tables/cells/select_cell.html', select_options=[],
                 select_param=None, select_pk_property='id', select_url=None, select_endpoint=None,
                 select_url_params=None, select_default_url_params=None, **kwargs):
        super(SelectMixin, self).__init__(name=name, cell_template=cell_template,
                                          select_options=select_options, select_param=select_param,
                                          select_pk_property=select_pk_property, select_url=select_url,
                                          select_endpoint=select_endpoint, select_url_params=select_url_params,
                                          select_default_url_params=select_default_url_params, **kwargs)
        self.select_options = select_options
        self.select_param = select_param
        self.select_pk_property = select_pk_property
        self.select_url = select_url
        self.select_endpoint = select_endpoint
        self.select_url_params = select_url_params
        self.select_default_url_params = select_default_url_params
        self.column_classes.append('cell-select')

    def get_select_classes(self, row, **kwargs):
        return ['select', 'btn-group', 'ui-select-dropdown']

    def get_select_attributes(self, row, **kwargs):
        attrs = {
            'data-resize': 'auto'
        }

        if self.select_param:
            attrs['data-param'] = self.select_param
        url = self.get_select_url(row, **kwargs)
        if url:
            attrs['data-url'] = url
        return attrs

    def get_selected_option(self, row, **kwargs):
        for option in self.select_options:
            if option.is_selected(self, row, **kwargs):
                return option
        return None

    def get_select_options(self, row, **kwargs):
        return self.select_options

    def get_select_url(self, row, **kwargs):
        return url_for_row(row=row, url=self.select_url, endpoint=self.get_select_endpoint(row, **kwargs),
                           url_params=self.get_select_url_params(row, **kwargs),
                           default_url_params=self.select_default_url_params)

    def get_select_endpoint(self, row, **kwargs):
        return self.select_endpoint

    def get_select_url_params(self, row, **kwargs):
        return self.select_url_params

    def get_cell_context(self, row=None, **kwargs):
        context = super(SelectMixin, self).get_cell_context(row=row, **kwargs)
        context.update({
            'selected_option': self.get_selected_option(row)
        })
        return context

    def get_cell_attributes(self, row, **kwargs):
        attrs = super(SelectMixin, self).get_cell_attributes(row=row, **kwargs)
        attrs['data-pjax-table-update-dropdown'] = True
        return attrs


class SelectOption(object):

    def __init__(self, value=None, display_value=None, icon=None, selected_display_value=None, name=None, **kwargs):
        self.value = value
        self.display_value = display_value or value
        self.selected_display_value = self.display_value if selected_display_value is None else selected_display_value
        self.icon = icon
        self.name = name

    def get_option_attributes(self, column, row, **kwargs):
        attrs = {
            'data-value': self.value,
            'data-name': self.name or column.property_str,
            'data-dropdown-option': True
        }
        if self.is_selected(column, row, **kwargs):
            attrs['data-selected'] = True
            attrs['selected'] = "selected"
        return attrs

    def is_selected(self, column, row, **kwargs):
        value = column.get_raw_value(row, **kwargs)
        return value == self.value
