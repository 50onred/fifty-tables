
class IconMixin(object):

    def __init__(self, name, cell_template='_tables/cells/icon_cell.html', icon_map={}, default_icon='',
                 default_icon_title=None, icon_attributes={}, icon_titles={}, **kwargs):
        super(IconMixin, self).__init__(name=name, cell_template=cell_template, icon_map=icon_map, icon_titles=icon_titles,
                                        default_icon=default_icon, default_icon_title=default_icon_title,
                                        icon_attributes=icon_attributes, **kwargs)
        self.icon_map = icon_map
        self.icon_titles = icon_titles
        self.default_icon = default_icon
        self.default_icon_title = default_icon_title
        self.icon_attributes = icon_attributes
        self.column_classes.append('cell-icon')

    def get_icon(self, row, **kwargs):
        return self.icon_map.get(self.get_raw_value(row), self.default_icon)

    def get_icon_attributes(self, row, **kwargs):
        attributes = {}
        attributes.update(self.icon_attributes)
        title = self.get_icon_title(row, **kwargs)
        if title:
            attributes.setdefault('title', title)
        return attributes

    def get_icon_title(self, row, **kwargs):
        return self.icon_titles.get(self.get_raw_value(row), self.default_icon_title)


class IconWithTextMixin(IconMixin):
    def __init__(self, name, cell_template='_tables/cells/icon_with_text_cell.html', **kwargs):
        super(IconWithTextMixin, self).__init__(name=name, cell_template=cell_template, **kwargs)
        self.column_classes.append('cell-icontext')

    def get_icon_text(self, row, **kwargs):
        return self.get_value(row, **kwargs)

