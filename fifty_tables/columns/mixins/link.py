from ...utils import url_for_row


class LinkMixin(object):

    def __init__(self, name, link_text=None, cell_template='_tables/cells/link_table_cell.html', url=None,
                 endpoint=None, url_params={}, default_url_params={}, **kwargs):
        super(LinkMixin, self).__init__(name=name, link_text=link_text, url=url, cell_template=cell_template,
                                        endpoint=endpoint, url_params=url_params, default_url_params=default_url_params,
                                        **kwargs)
        self.url = url
        self.link_text = link_text
        self.endpoint = endpoint
        self.url_params = url_params
        self.default_url_params = default_url_params
        self.column_classes.append('cell-link')

    def get_url(self, row):
        return url_for_row(row=row, url=self.url, endpoint=self.get_endpoint(row),
                           url_params=self.get_url_params(row), default_url_params=self.default_url_params)

    def get_endpoint(self, row):
        return self.endpoint

    def get_url_params(self, row):
        return self.url_params

    def get_link_text(self, row):
        if self.link_text is not None:
            return self.link_text
        return self.get_value(row)

    def get_link_attributes(self, row, **kwargs):
        return {}
