from .mixins import *
from .base import FiftyTableColumn


class NumericColumn(NumericMixin, FiftyTableColumn):
    """" Column with Numeric Mixin """


class CurrencyColumn(CurrencyMixin, FiftyTableColumn):
    """" Column with Currency Mixin """


class PercentageColumn(PercentageMixin, FiftyTableColumn):
    """ Column with percentage mixin """


class LinkColumn(LinkMixin, FiftyTableColumn):
    """ Column with Link Mixin """


class IconColumn(IconMixin, FiftyTableColumn):
    """ Column with Icon mixin """


class IconWithTextColumn(IconWithTextMixin, FiftyTableColumn):
    """ Column with Icon and Text """


class TooltipColumn(TooltipMixin, FiftyTableColumn):
    """ Column with Tooltip Mixin  """


class PropertyTooltipColumn(PropertyTooltipMixin, FiftyTableColumn):
    """ Column with property tooltip """


class CheckboxColumn(CheckboxMixin, FiftyTableColumn):
    """  Column with Checkbox Mixin """


class LinkButtonColumn(LinkButtonMixin, FiftyTableColumn):
    """ Column with LinkButton Mixin """


class EmailColumn(EmailMixin, FiftyTableColumn):
    """ Column with Email Mixin """


class DateColumn(DateMixin, FiftyTableColumn):
    """ Column with date mixin """


class DateTimeColumn(DateTimeMixin, FiftyTableColumn):
    """ Column with date time mixin """


class ImageColumn(ImageMixin, FiftyTableColumn):
    """ Column with ImageMixin """


class ClickableImageColumn(ClickableImageMixin, FiftyTableColumn):
    """ Column with Clickable Image Mixin  """


class EditableColumn(EmptyFooterMixin, EditableMixin, FiftyTableColumn):
    """ Column with Editable Mixin """


class EmptyFooterColumn(EmptyFooterMixin, FiftyTableColumn):
    """ Normal Column with no Footer """


class SelectColumn(SelectMixin, FiftyTableColumn):
    """ Column with SelectMixin """


class CurrencyMicrosColumn(MicrosMixin, CurrencyColumn):
    """ Column mixin to automatically convert from CPM Micros to CPM USD. """


class FiftyTableColumnWithHeaderTooltip(HeaderTooltipMixin, FiftyTableColumn):
    """ Table column with a tooltip in the header. """


class LinkColumnWithHeaderTooltip(HeaderTooltipMixin, LinkColumn):
    """ Link column with a tooltip in the header. """

class NumericColumnWithHeaderTooltip(HeaderTooltipMixin, NumericColumn):
    """ Numeric column with a tooltip in the header. """


class CurrencyColumnWithHeaderTooltip(HeaderTooltipMixin, CurrencyColumn):
    """ Currency column with a tooltip in the header. """


class PercentageColumnWithHeaderTooltip(HeaderTooltipMixin, PercentageColumn):
    """ Percentage column with a tooltip in the header. """
