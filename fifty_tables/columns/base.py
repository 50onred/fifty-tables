from collections import OrderedDict
from flask import current_app, template_rendered
from flask.templating import _default_template_ctx_processor
from flask_login import current_user


class FiftyTableColumn(object):

    def __init__(self, name, table=None, label=None, csv_label=None, sortable=True,
                 default_sort_direction='asc',
                 header_template='_tables/headers/base_table_header.html',
                 cell_template='_tables/cells/base_table_cell.html',
                 footer_template='_tables/footers/base_table_footer.html',
                 header_attributes={}, header_classes=[],
                 cell_attributes={}, cell_classes=[],
                 footer_attributes={}, footer_classes=[],
                 null_value='-', static_footer_value=None,
                 static_footer_colspan=None, csv_column=True, **kwargs):
        self.default_sort_direction = default_sort_direction
        self.cell_attributes = cell_attributes
        self.cell_classes = cell_classes
        self.cell_template = cell_template
        self.column_classes = []
        self.footer_attributes = footer_attributes
        self.footer_classes = footer_classes
        self.footer_template = footer_template
        self.header_attributes = header_attributes
        self.header_classes = header_classes
        self.header_template = header_template
        self.label = label if label is not None else name.replace('_', ' ').title()
        self.name = name
        self.null_value = null_value
        self.sortable = sortable
        self.static_footer_value = static_footer_value
        self.static_footer_colspan = static_footer_colspan
        self.table = table

        self.csv_label = csv_label if csv_label is not None else self.label
        self.csv_column = csv_column

    @property
    def property_str(self):
        return self.name

    def get_value(self, row, **kwargs):
        value = self.get_raw_value(row, **kwargs)
        if value is None:
            return self.null_value
        return value

    def get_raw_value(self, row, **kwargs):
        return row.get(self.property_str)

    def get_csv_value(self, row, **kwargs):
        return self.get_value(row, **kwargs)

    def get_footer_value(self, row, _row_idx=0, **kwargs):
        """ Gets the formatted value for the given footer row. This may return a static footer value if set. """
        value = self.get_static_footer_value(_row_idx=_row_idx, **kwargs)
        if value is None:
            value = self.get_value(row, **kwargs)

        return value

    def get_raw_footer_value(self, row, _row_idx=0, **kwargs):
        """ Gets the raw value for the given footer row. This may return a static footer value if set. """
        value = self.get_static_footer_value(_row_idx, **kwargs)
        if value is None:
            value = self.get_raw_value(row, **kwargs)

        return value

    def get_static_footer_value(self, _row_idx=0, **kwargs):
        """ Allows the chance to override the footer values with pre-supplied values at declaration """
        if isinstance(self.static_footer_value, dict):
            return self.static_footer_value.get(_row_idx, '')
        elif isinstance(self.static_footer_value, list):
            return self.static_footer_value[_row_idx] if _row_idx < len(self.static_footer_value) else ''
        elif self.static_footer_value is not None:
            return self.static_footer_value

    def get_base_context(self, **context):
        ctx = {}
        ctx.update(_default_template_ctx_processor())
        ctx['current_app'] = current_app
        ctx['current_user'] = current_user
        ctx.update(**context)
        ctx['column'] = self
        ctx['table'] = self.table
        return ctx

    def get_header_context(self, **context):
        return self.get_base_context(**context)

    def get_cell_context(self, **context):
        return self.get_base_context(**context)

    def get_footer_context(self, **context):
        return self.get_base_context(**context)

    def render_header(self, **context):
        context = self.get_header_context(**context)
        template = current_app.jinja_env.get_or_select_template(self.get_header_template(**context))
        rv = template.render(context)
        template_rendered.send(current_app._get_current_object(), template=template, context=context)
        return rv

    def render_cell(self, row, **context):
        context = self.get_cell_context(row=row, **context)
        template = current_app.jinja_env.get_or_select_template(self.get_cell_template(**context))
        rv = template.render(context)
        template_rendered.send(current_app._get_current_object(), template=template, context=context)
        return rv

    def render_footer(self, row, **context):
        context = self.get_footer_context(row=row, **context)
        template = current_app.jinja_env.get_or_select_template(self.get_footer_template(**context))
        rv = template.render(context)
        template_rendered.send(current_app._get_current_object(), template=template, context=context)
        return rv

    def get_header_template(self, **context):
        return self.header_template

    def get_cell_template(self, **context):
        return self.cell_template

    def get_footer_template(self, **context):
        return self.footer_template

    def get_header_attributes(self):
        attrs = {
            'data-property': self.property_str,
            'data-sortable': self.sortable,
            'data-default-sort-direction': self.default_sort_direction
        }
        if self.sortable and self.table and self.table.current_sort_property == self.property_str:
            attrs['data-current-sort'] = True
            attrs['data-current-sort-direction'] = self.table.current_sort_direction
        if self.header_attributes:
            attrs.update(self.header_attributes)
        return attrs

    def get_header_classes(self):
        return self._cell_classes(self.header_classes)

    def get_cell_attributes(self, row, **kwargs):
        attrs = {
            'data-property': self.property_str,
            'data-value': self.get_value(row, **kwargs)
        }
        if self.cell_attributes:
            attrs.update(self.cell_attributes)
        return attrs

    def get_cell_classes(self, row, **kwargs):
        return self._cell_classes(self.cell_classes)

    def get_footer_attributes(self, row, **kwargs):
        attrs = {}
        if self.footer_attributes:
            attrs.update(self.footer_attributes)

        if self.static_footer_colspan is not None:
            attrs['colspan'] = self.static_footer_colspan

        return attrs

    def get_footer_classes(self, row, **kwargs):
        return self._cell_classes(self.footer_classes, 'pjax-table-footer-static-content')

    def _cell_classes(self, *additional):
        items = self.column_classes or ['cell-text']
        for item in additional:
            if isinstance(item, list):
                items.extend(item)
            else:
                items.append(item)

        # Typically set() is fine, but we want to preserve order
        return list(OrderedDict.fromkeys(items))
