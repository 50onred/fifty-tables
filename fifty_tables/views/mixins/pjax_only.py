from flask import request
from ...result import FiftyTableResult


class PJAXOnlyTableMixin(object):
    table_pjax_only = True

    def get_table_result(self, params, **context):
        if self.table_pjax_only and 'X-PJAX' not in request.headers and params.get(self.csv_param) != "csv":
            # Disable fetching the real table results if we only care about pjax requests
            self.sort_property, self.sort_direction = self.get_sort_property_and_direction(self.params, **context)
            return FiftyTableResult(rows=[], total_rows=0, page=self.page, perpage=self.perpage,
                                    sort_property=self.sort_property, sort_direction=self.sort_direction)
        return super(PJAXOnlyTableMixin, self).get_table_result(params, **context)