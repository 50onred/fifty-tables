from flask import request, make_response
from six import iteritems

from ...utils import request_to_dict, result_to_list
from ...result import FiftyTableResult
from ...table import FiftyTable
from .persistence import PersistentTableMixin
from ...compat import base_string_type as basestring

class BaseTableMixin(object):
    """ Mixin to retrieve table & table data """
    pjax_template_name = '_tables/table.html'
    table_context_param = 'table'
    table_cls = FiftyTable
    table_classes = ['pjax-table', 'table']

    table_id = 'pjax-table'
    table_url = None
    table_template = '_tables/table.html'
    table_pjax_only = False
    table_auto_init = True
    paginated = True
    select_all_enabled = True
    push_state_enabled = True

    page = None
    perpage = None
    perpage_options = [50, 100, 500]
    default_page = 1
    default_perpage = 50

    sort_property = None
    sort_direction = None
    default_sort = None
    default_sort_direction = 'asc'

    params = {}
    default_params = {}
    url_params = {}

    table = None
    table_columns = None
    table_result = None
    table_rows = None
    table_footer_rows = None
    total_rows = 0

    csv_enabled = True
    csv_param = 'type'
    csv_filename = 'results.csv'

    def dispatch_request(self, *args, **kwargs):
        response = make_response(super(BaseTableMixin, self).dispatch_request(*args, **kwargs))
        if self.csv_enabled and request.args.get(self.csv_param) == 'csv' and self.table:
            response.mimetype = 'text/csv'
            response.headers['Content-Disposition'] = u'attachment;filename={}'.format(self.csv_filename)
        return response

    def get_context_data(self, **context):
        context = super(BaseTableMixin, self).get_context_data(**context)
        self.perpage_options = self.get_perpage_options(**context)
        self.params = self.get_params(**context)
        table = self.get_table(params=self.params, **context)
        if table:
            context[self.table_context_param] = table
        return context

    def render_response(self, **context):
        if self.csv_enabled and request.args.get(self.csv_param) == 'csv' and self.table:
            return self.table.render_csv(**context)
        return super(BaseTableMixin, self).render_response(**context)

    def get_table(self, params=None, **context):
        self.table_columns = self.get_table_columns(params=params, **context)
        self.table_result = self.get_table_result(params=params, **context)
        self.table = self.table_cls(id=self.table_id, columns=self.table_columns, results=self.table_result,
                                    pjax_url=self.table_url, default_sort=self.default_sort,
                                    template=self.table_template,
                                    paginated=self.paginated, perpage_options=self.perpage_options,
                                    perpage_min_threshold=min(self.perpage_options),
                                    select_all_enabled=self.select_all_enabled,
                                    push_state_enabled=self.push_state_enabled, pjax_only=self.table_pjax_only,
                                    auto_init=self.table_auto_init,
                                    attributes=self.get_table_attributes(params=params, **context),
                                    table_classes=self.get_table_classes(**context))
        return self.table

    def get_table_attributes(self, params=None, **context):
        """ Gets the additional attributes to include on the table """
        return {}

    def get_table_columns(self, params=None, **context):
        return []

    def get_table_result(self, params, **context):
        """ Builds the table result object. """
        self.sort_property, self.sort_direction = self.get_sort_property_and_direction(params, **context)
        results, total_rows = self.get_query_results_and_total(params, **context)
        self.table_rows = self.process_rows(result_to_list(results), params=params, **context)
        self.table_footer_rows = self.get_footer_rows(self.table_rows, params=params, **context)
        return FiftyTableResult(rows=self.table_rows, footer_rows=self.table_footer_rows,
                                total_rows=total_rows, page=self.page, perpage=self.perpage,
                                sort_property=self.sort_property, sort_direction=self.sort_direction)

    def get_sort_property_and_direction(self, params, **context):
        """ Parses the sort property / direction """
        if params.get('order'):
            order = params['order'].split('__')
            if len(order) == 2 and self.is_valid_sort_property(order[0]):
                return order[0], order[1]
        return self.default_sort, self.default_sort_direction

    def is_valid_sort_property(self, sort_property):
        return any(c.sortable and sort_property == c.name for c in self.table_columns)

    def get_perpage_options(self, **context):
        return self.perpage_options

    def get_params(self, **context):
        """ Parses the parameters needed to generate the correct table results """
        params = {}
        if self.default_sort and self.default_sort_direction:
            params['order'] = '{}__{}'.format(self.default_sort, self.default_sort_direction)

        if self.paginated and self.default_page is not None and self.default_perpage is not None:
            params['page'] = self.default_page
            params['perpage'] = self.default_perpage

        # Populate params with default values
        params.update(self.get_default_params(**context))

        # Populate params with the request values
        params.update(request_to_dict(request.values))

        # Populate params with the values from the URL (ex: /user/<int:id>)
        if isinstance(self.url_params, basestring):
            params[self.url_params] = self.kwargs.get(self.url_params)
        elif isinstance(self.url_params, list):
            for p in self.url_params:
                params[p] = self.kwargs.get(p)
        else:
            for k, v in iteritems(self.url_params):
                params[k] = self.kwargs.get(k, v)

        # Populate the params with the page / perpage info
        self.page, self.perpage = self.get_page_perpage(**context)
        if not self.paginated or (self.page is None and self.perpage is None):
            # Remove pagination
            if 'page' in params:
                del params['page']
            if 'perpage' in params:
                del params['perpage']
        else:
            params['page'] = self.page
            params['perpage'] = self.perpage

        return params

    def get_default_params(self, **context):
        return self.default_params

    def get_page_perpage(self, **context):
        """ Parse the page / perpage parameters """
        if self.csv_enabled and request.args.get(self.csv_param) == "csv":
            # Disable pagination for CSV downloads
            return None, None

        try:
            # Parse the perpage parameter from the cookie / request args
            perpage = self.default_perpage
            if request.cookies.get('pjax_table_perpage'):
                perpage = abs(int(request.cookies['pjax_table_perpage']))
            if request.args.get('perpage'):
                perpage = abs(int(request.args['perpage']))

            # Make sure we have a valid perpage
            if perpage not in self.perpage_options:
                perpage = self.default_perpage

            # Parse the page peramater
            page = int(request.args.get('page', self.default_page))

            # Make sure we have a page that's not 0
            page = max(page, self.default_page)
        except ValueError:
            page, perpage = self.default_page, self.default_perpage
        return page, perpage

    def get_query_results_and_total(self, params, **context):
        raise NotImplementedError()

    def process_rows(self, rows, params=None, **context):
        """ Method hook for post-processing rows """
        return rows

    def get_footer_rows(self, rows, params=None, **context):
        return []

    def get_table_classes(self, **context):
        return self.table_classes


class TableMixin(PersistentTableMixin, BaseTableMixin):
    """ Mixin to retrieve table & table data, optionally persisting and restoring choices. """
