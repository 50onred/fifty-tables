from flask import current_app, redirect, request, session, url_for
from werkzeug.local import LocalProxy


class TableStore(object):
    def __init__(self, enabled, columns_not_persisted=None):
        self.enabled = enabled
        self.columns_not_persisted = columns_not_persisted

    def load(self):
        """Only load from the store and respond if appropriate.
        """
        if self.should_load():
            return self.load_from_store()

    def persist(self, params):
        """Only persist when persistance is appropriate.
        """
        params = self.filter_params(params)
        if self.should_persist(params):
            self.persist_to_store(params)

    def should_load(self):
        """Check to see if we should load from storage.
        """
        return self.enabled and not request.args

    def should_persist(self, params):
        """Return True if we should persist, False otherwise.
        """
        return self.enabled and request.referrer

    def filter_params(self, params):
        """Return only the params that are allowed.
        """
        if not self.columns_not_persisted:
            return params
        return {column: params[column]
                for column in params if column not in self.columns_not_persisted}

    def load_from_store(self):
        """Retrieve any params from storage and process them. If a custom response is required,
        return it, otherwise return None.
        """

    def persist_to_store(self, params):
        """Persist to the store.
        """


class SessionTableStore(TableStore):
    session_key = 'ts'
    params_key = None

    def load_from_store(self):
        params = session.get(self.get_session_key(), {})
        endpoint_params = self.filter_params(params.get(self.get_params_key(), {}))

        if endpoint_params:
            values = {}
            values.update(endpoint_params)
            values.update(request.view_args)
            return redirect(url_for(request.endpoint, **values))

    def persist_to_store(self, params):
        store_params = session.setdefault(self.get_session_key(), {})
        store_params[self.get_params_key()] = params

    def get_params_key(self):
        return self.params_key or request.endpoint

    def get_session_key(self):
        return self.session_key


class PersistentTableMixin(object):
    """ Mixin to persist table data choices. """
    persist = None
    columns_not_persisted = {'q', 'page', 'start_date', 'end_date', 'date_range_name', 'timezone', 'type', '_pjax'}
    table_store_cls = SessionTableStore

    def get(self, *args, **kwargs):
        table_store = self.get_table_store()

        # Load from store and return a response
        response = table_store.load()
        if response:
            return response

        response = super(PersistentTableMixin, self).get(*args, **kwargs)

        # Persist changes
        table_store.persist(self.params)

        return response

    def get_table_store(self):
        table_store_cls = self.get_table_store_cls()
        return table_store_cls(self.persistence_enabled, self.get_columns_not_persisted())

    def get_table_store_cls(self):
        return self.table_store_cls

    def get_columns_not_persisted(self):
        return self.columns_not_persisted

    @property
    def persistence_enabled(self):
        return self.persist or self.persist is None and current_app.config.get('PERSISTENT_TABLES')
