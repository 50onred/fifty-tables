from ...columns.mixins import NumericMixin


class FooterTotalsMixin(object):

    compute_subtotals = True

    subtotals_row = None
    totals_row = None
    grand_totals_row = None

    def get_footer_rows(self, rows, params=None, **context):
        self.footer_rows = super(FooterTotalsMixin, self).get_footer_rows(rows, params=params, **context)

        for totals_method in (self.get_subtotals_row, self.get_totals_row, self.get_grand_totals_row):
            footer_row = totals_method(rows, params=params, **context)
            if footer_row is not None:
                self.footer_rows.append(footer_row)
        return self.footer_rows

    def get_subtotals_row(self, rows, params=None, **context):
        if not self.compute_subtotals:
            return None
        sum_columns = self.get_sum_columns(params=params, **context)
        subtotals_row = {property_str: 0 for property_str in sum_columns}

        for row in rows:
            for property_str in sum_columns:
                if row.get(property_str):
                    subtotals_row[property_str] += row[property_str]
        return subtotals_row

    def get_totals_row(self, rows, params=None, **context):
        return None

    def get_grand_totals_row(self, rows, params=None, **context):
        return None

    def get_sum_columns(self, params=None, **context):
        return [c.property_str for c in self.table_columns if isinstance(c, NumericMixin) and c.sum_page_totals]
