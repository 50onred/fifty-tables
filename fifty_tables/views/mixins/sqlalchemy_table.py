from decimal import Decimal
from datetime import date

from six import iteritems
from sqlalchemy import asc, desc
from sqlalchemy.orm import RelationshipProperty
import re
from .table import TableMixin
from ...result import FiftyTableResult


class SQLAlchemyTableMixin(TableMixin):
    """ Mixin to generate table results from a SQLAlchemy Query """
    keep_fields = []
    exclude_fields = []
    sort_attrs = {}
    sort_classes = []

    query = None
    filtered_query = None
    sorted_query = None
    paginated_query = None
    prefix_object_keys = False

    def get_table_result(self, params, **context):
        """ Builds the table result object. """
        self.sort_property, self.sort_direction = self.get_sort_property_and_direction(params, **context)
        self.query = self.get_query(params, **context)
        self.filtered_query = self.apply_filters_to_query(self.query, params, **context)

        self.sorted_query = self.apply_sort_to_query(self.filtered_query, self.sort_property, self.sort_direction, params=params, **context)
        self.paginated_query = self.apply_pagination_to_query(self.sorted_query, self.page, self.perpage, params=params, **context)

        # Retrieve the table rows
        self.table_rows = self.process_rows([row for row in self.generate_table_rows(self.paginated_query, params=params, **context)], params=params, **context)

        # Retrieve the footer rows
        self.table_footer_rows = self.get_footer_rows(self.table_rows, params=params, **context)

        # Calculate how many rows are in the result set
        self.total_rows = self.get_total_rows(self.filtered_query, self.table_rows, self.page, self.perpage, params=params, **context)

        return FiftyTableResult(rows=self.table_rows, footer_rows=self.table_footer_rows,
                                total_rows=self.total_rows, page=self.page, perpage=self.perpage,
                                sort_property=self.sort_property, sort_direction=self.sort_direction)

    def get_query(self, params, **context):
        raise NotImplementedError()

    def apply_filters_to_query(self, query, params, **context):
        """ Applies filters to the specified query """
        return query

    def apply_sort_to_query(self, query, sort_property, sort_direction, params=None, **context):
        """ Applies the order clause to the given query. """
        if sort_property and sort_direction and not re.search('[^a-zA-Z0-9_]', sort_property):
            sort_func = desc if sort_direction == 'desc' else asc
            sort_attrs = self.get_sort_attrs(params=params, **context)
            # Check to see if we have a static mapping (ex. 'user': User.last_name)
            if sort_attrs and sort_property in sort_attrs and sort_attrs[sort_property] is not None:
                return query.order_by(sort_func(sort_attrs[sort_property]))

            # Check to see if we need to include the sort class on the query
            sort_classes = self.get_sort_classes(params=params, **context)
            if sort_classes:
                for cls in sort_classes:
                    if hasattr(cls, sort_property):
                        return query.order_by(sort_func(getattr(cls, sort_property)))

            # Use property name
            return query.order_by(sort_func(sort_property))
        return query

    def get_sort_attrs(self, params=None, **context):
        return self.sort_attrs or {}

    def get_sort_classes(self, params=None, **context):
        return self.sort_classes or []

    def apply_pagination_to_query(self, query, page, perpage, params=None, **context):
        """ Applies pagination to the given query """
        if self.paginated and page is not None and perpage is not None:
            return query.limit(perpage).offset((page - 1) * perpage)
        return query

    def get_total_rows(self, query, rows, page, perpage, **context):
        """ Determines the number of rows for the table """
        if page == 1 and len(rows) < perpage:
            # Avoid querying the database if we have less than the requested number of items
            return len(rows)
        return query.count()

    def get_footer_rows(self, rows, **context):
        return []

    def generate_table_rows(self, query, params=None, **context):
        """ Generator method for table rows """
        for obj in query:
            yield self.object_to_dict(obj, params=params, **context)

    def object_to_dict(self, obj, params=None, **context):
        """
        Converts a SQLAlchemy object to a flat dictionary.

        :param obj: The SQLAlchemy model or result object
        :param params: The parameters from the table view.
        :return: A dictionary object with the form { 'property or label': value }
        """
        def mutate(obj_dict):
            mutated = {}
            for key, item in iteritems(obj_dict):
                if self.keep_fields and key not in self.keep_fields:
                    continue
                elif self.exclude_fields and key in self.exclude_fields:
                    continue
                elif key.startswith('_'):
                    continue
                elif hasattr(obj.__class__, key) and hasattr(getattr(obj.__class__, key), 'property') \
                        and isinstance(getattr(obj.__class__, key).property, RelationshipProperty):
                    # Ignore relationship properties automatically
                    continue
                elif hasattr(item, '_sa_instance_state'):
                    # Sub-items within a result object
                    prefix = self.get_object_key_prefix(obj, key, item, params=params, **context)
                    mutated.update({prefix + k: v for k, v in iteritems(mutate(item.__dict__))})
                    continue
                elif isinstance(item, Decimal):
                    item = float(item)
                mutated[key] = item
            return mutated
        d = obj._asdict() if hasattr(obj, '_asdict') else obj.__dict__
        return mutate(d)

    def get_object_key_prefix(self, obj, key, item, params=None, **context):
        return '{}_'.format(key.lower()) if self.prefix_object_keys else ''
