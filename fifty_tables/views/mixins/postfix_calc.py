import operator
from fifty_tables.compat import PY2, base_string_type as basestring
from six import iteritems


class BasePostfixCalcRowsMixin(object):
    """
    Applies calculations to table rows. Example:

    calc_fields = {
        'ctr': ['clicks', 'impressions', '/', 100, '*'],
        'avg_cpc': ['spend', 'clicks', operators.div],
        'avg_cpv': ['spend', 'impressions', '/'],
        'cpm': ['spend', 'impressions', '/', 1000, '*'],
        'display_position': ['display_position', 'impressions', '/'],
        'epc': ['conversions', 'clicks', '/'],
        'fraud_percent': ['fraud_events', 'impressions', '/', 100, '*']
    }
    """
    operator_functions = {
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul,
        '/': operator.div if PY2 else operator.truediv,
        '%': operator.mod
    }
    divide_by_zero_val = None

    def apply_calculations(self, row):
        """ Applies calculations to the given row. """
        for attr, operations in iteritems(self.get_row_calc_fields(row)):
            row[attr] = self.postfix(row, operations)
        return row

    def postfix(self, data, input):
        stack = []
        for v in input:
            if v in self.operator_functions:
                second = stack.pop()
                first = stack.pop()
                if isinstance(second, basestring):
                    second = data[second]
                if isinstance(first, basestring):
                    first = data[first]
                val = self.operate(first, second, v)
                stack.append(val)
            else:
                stack.append(v)
        return stack.pop()

    def operate(self, a, b, op):
        try:
            return self.operator_functions[op](self.null_to_zero(a), self.null_to_zero(b)) if isinstance(op, basestring) \
                else op(self.null_to_zero(a), self.null_to_zero(b))
        except ZeroDivisionError:
            return self.divide_by_zero_val

    def null_to_zero(self, val):
        return val or 0

    def get_row_calc_fields(self, row):
        return {}


class PostfixCalcProcessRowsMixin(BasePostfixCalcRowsMixin):

    def process_rows(self, rows, **context):
        rows = super(PostfixCalcProcessRowsMixin, self).process_rows(rows, **context)
        for row in rows:
            self.apply_calculations(row)
        return rows


class PostfixCalcSubtotalsRowMixin(BasePostfixCalcRowsMixin):

    def get_subtotals_row(self, rows, params=None, **context):
        subtotals_row = super(PostfixCalcSubtotalsRowMixin, self).get_subtotals_row(rows, params=params, **context)
        if subtotals_row:
            self.apply_calculations(subtotals_row)
        return subtotals_row
