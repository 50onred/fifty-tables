try:
    from fifty_flask.views.generic  import TemplateView, FormView
except ImportError:
    from fifty.flask.views.generic import TemplateView, FormView
from .mixins import TableMixin, SQLAlchemyTableMixin, PJAXOnlyTableMixin


class TableView(TableMixin, TemplateView):
    """ Template View with table mixin """


class TableWithFormView(TableMixin, FormView):
    """ Form View with table mixin """


class SQLAlchemyTableView(SQLAlchemyTableMixin, TemplateView):
    """ TableView generated from a SQLAlchemy Query """


class SQLAlchemyTableWithFormView(SQLAlchemyTableMixin, FormView):
    """ Form View with table mixin generated from a SQLAlchemy Query """


class PJAXOnlyTableView(PJAXOnlyTableMixin, TableView):
    """ Table view that only queries for results on PJAX Requests. Useful if queries may take a long time. """


class PJAXOnlySQLAlchemyTableView(PJAXOnlyTableMixin, SQLAlchemyTableView):
    """ SQLAlchemy Table view that only queries for results on PJAX Requests. Useful if queries may take a long time. """


class PJAXOnlyTableWithFormView(PJAXOnlyTableMixin, TableWithFormView):
    """ PJAX-only table view with form """


class PJAXOnlySQLAlchemyTableWithFormView(PJAXOnlyTableMixin, SQLAlchemyTableWithFormView):
    """ PJAX-only SQLAlchemy table view with form """
