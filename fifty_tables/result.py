from math import ceil


class FiftyTableResult(object):

    def __init__(self, rows=[], footer_rows=[], page=None, perpage=None, total_rows=None,
                 sort_property=None, sort_direction=None):
        self.rows = rows
        self.footer_rows = footer_rows
        self.page = page
        self.perpage = perpage
        self.total_rows = total_rows
        self.sort_property = sort_property
        self.sort_direction = sort_direction

    @property
    def total_pages(self):
        if self.perpage and self.total_rows:
            return int(ceil(self.total_rows / float(self.perpage)))
        return 1

    @property
    def start_row(self):
        if not self.total_rows:
            return 0
        return (self.page - 1) * self.perpage + 1

    @property
    def end_row(self):
        return max(self.start_row + len(self.rows) - 1, 0)