try:
    from fifty_flask.blueprints import Blueprint
except ImportError:
    from fifty.flask.blueprints import Blueprint
from six import iteritems


default_config = {
    'FIFTY_TABLES_BLUEPRINT_NAME': 'tables',
    'FIFTY_TABLES_URL_PREFIX': None
}


class FiftyTables(object):

    def __init__(self, app=None):
        self.app = app
        self.blueprint = None
        self._state = None
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.app = app

        for key, value in iteritems(default_config):
            app.config.setdefault(key, value)

        self.blueprint = Blueprint(app.config['FIFTY_TABLES_BLUEPRINT_NAME'], __name__,
                                   url_prefix=app.config['FIFTY_TABLES_URL_PREFIX'])
        app.register_blueprint(self.blueprint)
