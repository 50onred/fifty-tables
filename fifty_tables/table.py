from flask import current_app, template_rendered
from flask.templating import _default_template_ctx_processor
from flask_login import current_user
from .compat import buffer_type, base_string_type as basestring, PY2
import csv


class FiftyTable(object):

    def __init__(self, id='pjax-table', columns=[], attributes={}, pjax_url=None, results=None, paginated=True,
                 perpage_options=None, perpage_min_threshold=None,
                 default_sort=None, auto_init=True, push_state_enabled=True,
                 select_all_enabled=True, pjax_only=False,
                 template='_tables/table.html',
                 table_loading_message='Loading your data...',
                 table_empty_message="Whoops! Looks like there's nothing in this table!",
                 table_classes=[], **kwargs):
        self.id = id
        self.pjax_url = pjax_url
        self.columns = []
        self.attributes = attributes or {}
        self.results = results
        self.paginated = paginated
        self.perpage_options = perpage_options
        self.perpage_min_threshold = perpage_min_threshold
        self.template = template
        self.default_sort = default_sort
        self.auto_init = auto_init
        self.push_state_enabled = push_state_enabled
        self.select_all_enabled = select_all_enabled
        self.pjax_only = pjax_only
        self.table_loading_message = table_loading_message
        self.table_empty_message = table_empty_message
        self.add_columns(*columns)
        self.table_classes = table_classes

    @property
    def current_sort_property(self):
        if self.results and hasattr(self.results, 'sort_property'):
            return self.results.sort_property or self.default_sort
        return self.default_sort

    @property
    def current_sort_direction(self):
        if self.results and hasattr(self.results, 'sort_direction'):
            return self.results.sort_direction
        return None

    def add_columns(self, *columns):
        """ Adds a column to the table, setting a reference back to itself """
        for column in columns:
            column.table = self
            self.columns.append(column)
        return self

    def get_context(self, **context):
        ctx = {}
        ctx.update(_default_template_ctx_processor())
        ctx['current_user'] = current_user
        ctx.update(context)
        ctx['table'] = self
        return ctx

    def render_table(self, **context):
        context = self.get_context(**context)
        template = current_app.jinja_env.get_or_select_template(self.template)
        rv = template.render(context)
        template_rendered.send(current_app._get_current_object(), template=template, context=context)
        return rv

    def get_attributes(self):
        attrs = {
            'data-current-sort-property': self.current_sort_property,
            'data-current-sort-direction': self.current_sort_direction
        }
        if self.results.total_rows is not None:
            attrs['data-total-rows'] = self.results.total_rows
        if self.paginated and self.results:
            attrs['data-current-page'] = self.results.page
            attrs['data-current-perpage'] = self.results.perpage
        attrs.update(self.attributes)
        return attrs

    def get_container_attributes(self):
        return {
            'data-pjax-table-id': self.id,
            'data-pjax-url': self.pjax_url,
            'data-paginated': self.paginated,
            'data-push-state-enabled': self.push_state_enabled,
            'data-select-all-enabled': self.select_all_enabled,
            'data-pjax-only': self.pjax_only,
            'data-auto-init': self.auto_init
        }

    def render_csv(self, **context):
        csv_buff = buffer_type()
        writer = csv.writer(csv_buff)
        writer.writerow([c.csv_label for c in self.columns if c.csv_column])
        for row in self.results.rows:
            values = []
            for c in self.columns:
                if c.csv_column:
                    val = c.get_csv_value(row, **context)
                    if PY2 and isinstance(val, basestring):
                        val = val.encode('utf-8')
                    values.append(val)
            writer.writerow(values)
        return csv_buff.getvalue()

    def get_table_classes(self, **context):
        return self.table_classes
