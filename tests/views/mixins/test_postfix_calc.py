from unittest.case import TestCase
from fifty_tables.views.mixins.postfix_calc import BasePostfixCalcRowsMixin


def get_test_data():
    return {
        'x': 10,
        'y': 20,
        'z': 30
    }


class TestPostfixCalc(TestCase):

    def setUp(self):
        self.calc = BasePostfixCalcRowsMixin()
        self.data = get_test_data()

    def test_add_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'y', '+'])
        self.assertEquals(value, 30)

    def test_subtract_postfix(self):
        value = self.calc.postfix(self.data, ['y', 'x', '-'])
        self.assertEquals(value, 10)

    def test_multiply_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'y', '*'])
        self.assertEquals(value, 200)

    def test_divide_postfix(self):
        value = self.calc.postfix(self.data, ['y', 'x', '/'])
        self.assertEquals(value, 2)

    def test_multi_add_postfix(self):
        value = self.calc.postfix(self.data, ['y', 'x', '+', 'z', '+'])
        self.assertEquals(value, 60)

    def test_multi_subtract_postfix(self):
        value = self.calc.postfix(self.data, ['z', 'x', '-', 'x', '-'])
        self.assertEquals(value, 10)

    def test_multiply_add_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'x', '*', 'y', '+'])
        self.assertEquals(value, 120)

    def test_multiply_subtract_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'x', '*', 'y', '-'])
        self.assertEquals(value, 80)

    def test_divide_add_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'x', '/', 'y', '+'])
        self.assertEquals(value, 21)

    def test_divide_subtract_postfix(self):
        value = self.calc.postfix(self.data, ['z', 'x', '/', 'x', '-'])
        self.assertEquals(value, -7)

    def test_multiply_divide_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'y', '*', 'x', '/'])
        self.assertEquals(value, 20)

    def test_complex_postfix(self):
        value = self.calc.postfix(self.data, ['x', 'x', '+', 'x', '*', 'y', '-', 'x', 'x', '+', '/'])
        self.assertEquals(value, 9)

    def test_divide_by_zero(self):
        value = self.calc.postfix(self.data, ['x', 0, '/'])
        self.assertIsNone(value)
